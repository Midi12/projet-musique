from .app import manager, db
from .models import Author, Album, Genre, AssocGenreAlbum, AssocPlaylistAlbum
import yaml

@manager.command
def loaddb(filename):
	"""
	Populates the database with the provided yaml file.
	"""
	db.create_all()

	#chargement jeu de données
	albums = yaml.load(open(filename))

	#creation des auteurs
	parsed_authors = {}

	for s in albums:
		aut = s['by']
		if aut not in parsed_authors:
			o = Author(_name = aut)
			db.session.add(o)
			parsed_authors[aut] = o

	db.session.commit()

	#creation des genres
	parsed_genres = {}

	for s in albums:
		gen = s['genre']
		for g in gen:
			if g not in parsed_genres:
				o = Genre(_name = g)
				db.session.add(o)
				parsed_genres[g] = o

	db.session.commit()

	#creation des chansons
	for s in albums:
		a = parsed_authors[s['by']]
		o = Album(_id = s['entryId'], _title = s['title'], _year = s['releaseYear'], _img = s['img'], _is_absolute_path = False, _parent = s['parent'], _author_id = a.getId())
		db.session.add(o)
		parsed_genres_album = []
		for g in s['genre']:
			if g not in parsed_genres_album:
				assoc = AssocGenreAlbum(_album_id=o.getId(), _genre_id=parsed_genres[g].getId())
				db.session.add(assoc)
				parsed_genres_album.append(g)

	db.session.commit()
