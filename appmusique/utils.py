from .app import db, login_manager
from .models import *
from sqlalchemy import update
from flask import flash, redirect, url_for

def get_albums(range=10):
    return Album.query.order_by(func.random()).limit(range).all()

def get_artists(range=10):
	return Author.query.order_by(func.random()).limit(range).all()

def get_album(id):
    return Album.query.get(id)

def get_artist_name(artid):
    return Author.query.get(artid).getName()

def get_artist(artid):
    return Author.query.get(artid)

def get_artist_albums(artid):
	return Album.query.filter(Album._author_id == artid).all()

def get_genres_album(id):
	assocs = AssocGenreAlbum.query.filter(AssocGenreAlbum._album_id == id).all()
	genres = []
	for a in assocs:
		genres.append(a.getGenre())
	return genres

def get_albums_playlist(id):
	assocs = AssocPlaylistAlbum.query.filter(AssocPlaylistAlbum._playlist_id == id).all()
	albums = []
	for a in assocs:
		albums.append(a) # a.getTitle()
	return albums

def get_user_playlists(id):
	return Playlist.query.filter(Playlist._user_id == id).all()

def get_user(id):
	return User.query.get(id)

def add_playlist_script(name, uid):
	pl = Playlist(_name=name, _user_id=uid)
	db.session.add(pl)
	db.session.commit()
	return redirect(url_for("playlists"))

def add_album_impl(title, year, img, compo, genres, artid):
	al = Album(_title = title, _year = year, _img = img, _is_absolute_path = True, _parent = compo, _author_id = artid)
	db.session.add(al)
	db.session.commit()
	parsed_genres = ""
	for c in genres:
		if c != ' ':
			parsed_genres += c
	parsed_genres = parsed_genres.split(',')
	for genre in parsed_genres:
		g = Genre.query.filter(Genre._name == genre).first()
		if g == None:
			g = Genre(_name=genre)
			db.session.add(g)
			db.session.commit()
		assoc = AssocGenreAlbum(_album_id=al.getId(), _genre_id=g.getId())
		db.session.add(assoc)
		db.session.commit()
	return redirect(url_for("artist", id=artid))

def search_artist_from_name(name):
	return Author.query.filter(Author._name.like("%" + name + "%")).all()

def search_album_from_name(name):
	return Album.query.filter(Album._title.like("%" + name + "%")).all()

def search_user_from_name(name):
	return User.query.filter(User._nickname.like("%" + name + "%")).all()

def search_playlist_from_name(name):
	return Playlist.query.filter(Playlist._name.like("%" + name + "%")).all()

def register_new_user(nickname, password):
	user = User(_nickname = nickname, _password = password, _level = u'member')
	db.session.add(user)
	db.session.commit()

def modify_user_impl(id, npass):
	user = User.query.filter(User._id == id).first()
	if user is None:
		flash('User does not exists' , 'error')
		flash('Cannot modify credentials' , 'error')
		return redirect(url_for('home'))

	if npass is None or npass == "":
		flash('Bad data' , 'error')
		flash('Cannot modify credentials' , 'error')
		return redirect(url_for('home'))

	flash('id = ' + str(id) + ' npass = ' + npass, 'warning')

	#update(User).where(User._id == id).values(_password = npass)
	db.session.query(User).filter(User._id == id).update({'_password': npass})
	db.session.commit()
	flash('Credentials modified successfully')
	return redirect(url_for('usercp'))

def modify_album_impl(id, title, year, compo):
	album = Album.query.get(id)
	if album is None:
		flash('User does not exists' , 'error')
		#flash('Cannot modify credentials' , 'error')
		return redirect(url_for('home'))
	if title == "":
		title = album.getTitle()
	if year == "":
		year = album.getYear()
	else:
		year = int(year)
	if compo == "":
		compo = album.getParent()
	db.session.query(Album).filter(Album._id == id).update({'_title': title, '_year': year, '_parent': compo})
	db.session.commit()
	flash('Album details modified successfully')
	return redirect(url_for('album', id=id))

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))

def add_album_to_playlist_script(pid, aid):
	assoc = AssocPlaylistAlbum(_album_id = aid, _playlist_id = pid)
	db.session.add(assoc)
	db.session.commit()
