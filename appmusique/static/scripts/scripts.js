$(window).load(function(){
	$('.do2g-container').find('img').each(function(){
		var imgClass = (this.width/this.height > 1) ? 'wide' : 'tall';
		$(this).addClass(imgClass);
	})
})

function remove_flash_msg(i)
{
	//alert(i);
	//alert("#flmsg" + i);
	$("#flmsg" + i.toString()).remove();
}

function showUserPanel()
{
	if ($('#userPanel').hasClass('hidden'))
	{
		var pos = $("#userButton").offset();
		$("#userPanel").removeClass("hidden");
		$("#userPanel").offset({ top: pos.top + 35, left: pos.left - $("#userPanel").width() + 25 });
	}
	else {
		hideUserPanel();
	}
}

function hideUserPanel()
{
	$("#userPanel").addClass("hidden");
}

function toggle_display_add_form()
{
	if ($('#add-form').hasClass('hidden'))
	{
		$('#add-form').removeClass('hidden');
		$('#add-button').html('Hide');
		$('input').each(function(){
			$(this).val('');
		});
	}
	else
	{
		$('#add-form').addClass('hidden');
		$('#add-button').html('Add album');
	}
}

function toggle_display_create_form()
{
	if ($('#create-from').hasClass('hidden'))
	{
		$('#create-from').removeClass('hidden');
		$('#create-button').html('Hide');
		$('input').each(function(){
			$(this).val('');
		});
	}
	else
	{
		$('#create-from').addClass('hidden');
		$('#create-button').html('Create playlist');
	}
}

function toggle_display_edit_form(title, year, compo)
{
	if ($('#edit-form').hasClass('hidden'))
	{
		$('#album-title').val(title);
		$('#album-year').val(year);
		$('#album-compositor').val(compo);
		$('#edit-form').removeClass('hidden');
	}
	else
	{
		$('#edit-form').addClass('hidden');
	}
}

function toggle_add_album_to_playlist_form()
{
	if ($('#add-form').hasClass('hidden'))
		$('#add-form').removeClass('hidden');
	else
		$('#add-form').addClass('hidden');
}

function set_add_from_hidden_field(id)
{
	$('#add-form a').each(function(){
		$(this).removeClass('clicked');
	})
	$('#pid').val(id);
	$('#a' + id).addClass('clicked');
}
